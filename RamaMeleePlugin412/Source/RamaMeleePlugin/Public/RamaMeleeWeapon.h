// Copyright 2015 by Nathan "Rama" Iyer. All Rights Reserved.
#pragma once

//Core
#include "RamaMeleeCore.h"
 
#include "Components/SkeletalMeshComponent.h"
#include "RamaMeleeWeapon.generated.h"
  
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams( FRamaMeleeHitSignature, class AActor*, HitActor, class UPrimitiveComponent*, HitComponent, const FVector&, ImpactPoint, const FVector&, ImpactNormal, FName, HitBoneName, const struct FHitResult&, HitResult );
 
UCLASS(ClassGroup=Rama, meta=(BlueprintSpawnableComponent))
class RAMAMELEEPLUGIN_API URamaMeleeWeapon : public USkeletalMeshComponent
{
	GENERATED_BODY()
public:
	URamaMeleeWeapon(const FObjectInitializer& ObjectInitializer); 
	
	UPROPERTY(EditAnywhere, Category="Rama Melee Weapon")
	TArray<TEnumAsByte<EObjectTypeQuery> > MeleeTraceObjectTypes;
	 
	/**If HitWasDamage returns false, then the weapon hit another object with a non-damaging shape. Returns false if no hit at all. */
	UPROPERTY(BlueprintAssignable, Category="Rama Melee Weapon")
	FRamaMeleeHitSignature RamaMeleeWeapon_OnHit;
	
	/** Draw the PhysX Shapes, indicating which ones do damage! <3 Rama */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Rama Melee Weapon ~ Draw")
	bool DrawShapes = true;
	 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Rama Melee Weapon ~ Draw")
	bool DrawSweeps = true;
	 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Rama Melee Weapon ~ Draw")
	bool DrawLines = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Rama Melee Weapon ~ Draw")
	float DrawShapes_Thickness = 3;
	 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Rama Melee Weapon ~ Draw")
	float DrawShapes_Duration = 2;
	
	/** Each Body can have many shapes. BodyIndex specifies which body you want to provide shape indicies for. Each shape whose index is supplied here will be treated as a damaging part of the weapon. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Rama Melee Weapon")
	FRamaMeleeDamageMap DamageMap;
	
//~~~~~~~~~~~~~
// Core Functions
//~~~~~~~~~~~~~
public:
	UFUNCTION(Category="Rama Melee Weapon",BlueprintCallable)
	void StartSwingDamage();
	
	UFUNCTION(Category="Rama Melee Weapon",BlueprintCallable)
	void StopSwingDamage();
	
//~~~~~~~~~~~~~
// Core Utility
//~~~~~~~~~~~~~
public:
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category="Rama Melee Weapon")
	bool DoingSwingTraces = false;
	 
	/** Returns false if no hit at all. */
	UFUNCTION(Category="Rama Melee Weapon",BlueprintCallable)
	bool MeleeSweep(FHitResult& Hit, const TArray<FTransform>& BodyPreviousPose);
	
	TArray<FTransform> SwingPrevPose;
	void SwingTick();
	
public:
	void Draw();
	 
	FORCEINLINE bool IsValid() const
	{
		return SkeletalMesh != nullptr;
	}
	
	virtual void InitializeComponent() override;
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//Tick
	//		By using tick to draw, it shows up in Editor Preview! Woohoo!
	//			-Rama
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override
	{
		Super::TickComponent(DeltaTime,TickType,ThisTickFunction);
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
		if(DoingSwingTraces)
		{
			SwingTick();
		}
		 
		//Draw
		if(DrawShapes)
		{  
			Draw();
		}
	}
	
	/*
	FString ECRToString(ECollisionResponse ECR) const
	{
		switch (ECR)
		{
			case ECR_Block : return "Block";
			case ECR_Overlap : return "Overlap";
			case ECR_Ignore : return "Ignore";
		}
		return "None";
	}
	*/
	
};



